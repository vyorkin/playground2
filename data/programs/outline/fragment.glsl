uniform sampler2D mytexture;
uniform vec4 color;
uniform vec2 step_size;

varying vec2 f_texcoord;

void main(void) {
  vec4 texture = texture2D(mytexture, f_texcoord);

  float alpha = 4.0 * texture.a;

  alpha -= texture2D(mytexture, f_texcoord + vec2(step_size.x,       0.0)).a;
  alpha -= texture2D(mytexture, f_texcoord + vec2(1.0 - step_size.x, 0.0)).a;
  alpha -= texture2D(mytexture, f_texcoord + vec2(0.0,               step_size.y)).a;
  alpha -= texture2D(mytexture, f_texcoord + vec2(0.0,               1.0 - step_size.y)).a;

  gl_FragColor = vec4(color.xyz, color.a * alpha);
}
