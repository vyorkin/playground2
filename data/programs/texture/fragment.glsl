uniform sampler2D mytexture;

varying vec2 f_texcoord;

void main(void) {
  gl_FragColor = texture2D(mytexture, f_texcoord);
}
