varying vec2 texCoord;
varying vec2 pixelCoord;

uniform sampler2D tileset;
uniform sampler2D layermap;

uniform vec2 tilesetSize;
uniform vec2 tilesetSizeTiles;
uniform vec2 mapSizeTiles;

uniform vec2 inverseTileTextureSize;
uniform vec2 inverseSpriteTextureSize;
uniform float tileSize;
uniform float repeatTiles; // XXX: int? bool?

void main(void) {
  if (repeatTiles == 0.0 && (texCoord.x < 0.0 || texCoord.x > 1.0 || texCoord.y < 0.0 || texCoord.y > 1.0)) { discard; }
  vec4 tile = texture2D(layermap, texCoord);
  // vec3 tile = texture2D(layermap, texCoord).xyz;
  if (tile.z == 1.0) { discard; }

  vec2 spriteOffset = floor(tile.xy * 256.0) * tileSize;
  vec2 spriteCoord = mod(pixelCoord, tileSize);
  gl_FragColor = texture2D(tileset, (spriteOffset + spriteCoord) * inverseSpriteTextureSize);
  // gl_FragColor = vec4(
  //   (spriteOffset + spriteCoord) / tilesetSize,
  //   0.0,
  //   1.0
  // );

  // vec2 uv = fract(f_texcoord.xy * mapSizeTiles) / tileSize * 2.0;
  // vec2 tileOff = tile.xy * 256.0 / tilesetSizeTiles;
  // gl_FragColor = texture2D(tileset, uv + tileOff);

}
