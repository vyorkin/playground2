module Main where

import Control.Monad.IO.Class (MonadIO(..))
import Data.Foldable (for_)
import Data.StateVar (($=))
import Linear (R2, V2(..), V4(..), _xy, _zw, (^*))
import Control.Lens ((^.))
import System.Mem (performGC)

import qualified Apecs
import qualified Data.Vector.Storable as Vector
import qualified Graphics.Rendering.OpenGL as GL
import qualified SDL

import qualified Components
import qualified Components.Programs as Programs
import qualified Components.Textures as Textures
import qualified Lib

main :: IO ()
main = do
  SDL.initializeAll

  SDL.HintRenderScaleQuality $= SDL.ScaleNearest -- Linear
  window <- SDL.createWindow "Playground 2" glWindow
  SDL.showWindow window

  _glContext <- SDL.glCreateContext window
  SDL.swapInterval $= SDL.SynchronizedUpdates
  GL.blend $= GL.Enabled
  GL.blendFunc $= (GL.SrcAlpha, GL.OneMinusSrcAlpha)

  world <- Components.initWorld
  Apecs.runWith world $ do
    Textures.loadAll
    Programs.loadAll

    Apecs.set Apecs.global $ Components.Position 0

    mainLoop window

  SDL.destroyWindow window
  SDL.quit

glWindow :: SDL.WindowConfig
glWindow = SDL.defaultWindow
  { SDL.windowInitialSize     = V2 1600 900
  , SDL.windowGraphicsContext = SDL.OpenGLContext SDL.defaultOpenGL
  }

mainLoop :: SDL.Window -> Apecs.SystemT Components.World IO ()
mainLoop window = do
  _beginLoop <- SDL.ticks
  events <- SDL.pollEvents

  for_ events $ \e -> case SDL.eventPayload e of
    SDL.MouseMotionEvent SDL.MouseMotionEventData{SDL.mouseMotionEventPos=(SDL.P winPos)} ->
      Apecs.set Apecs.global $
        Components.Position $
          fmap fromIntegral winPos
    _ ->
      pure ()

  screenSize@(V2 width height) <- SDL.glGetDrawableSize window
  let
    halfWidth  = fromIntegral $ width `div` 2
    halfHeight = fromIntegral $ height `div` 2

  _midLoop <- SDL.ticks

  liftIO $ do
    GL.loadIdentity
    GL.ortho
      (negate halfWidth)
      halfWidth
      (negate halfHeight)
      (halfHeight)
      0
      (-100)

    GL.clearColor $= GL.Color4 0 0 0.5 1
    GL.clear [GL.ColorBuffer] -- XXX: depth, etc.?
    GL.viewport $=
      ( GL.Position 0 0
      , GL.Size (fromIntegral width) (fromIntegral height)
      )

  draw $ fmap fromIntegral screenSize

  SDL.glSwapWindow window

  if SDL.QuitEvent `elem` map SDL.eventPayload events then
    pure ()
  else do
    _endLoop <- SDL.ticks
    -- Lib.debugM $ "Mid time: " <> show (midLoop - beginLoop)
    -- Lib.debugM $ "Loop time: " <> show (endLoop - beginLoop)
    liftIO performGC
    mainLoop window

draw :: V2 Float -> Apecs.SystemT Components.World IO ()
draw screenSize = do
  ms <- SDL.ticks
  let seconds = fromIntegral ms / (1000 :: Float)

  solid (GL.Vector4 0.2 0 0.2 0.3) $ \coord2d ->
    liftIO . GL.preservingMatrix $ do
      GL.rotate 45 $ GL.Vector3 @Float 0 0 (-1)
      Lib.drawQuads coord2d $
        Lib.quad 1000 1000 0 0

  fancy seconds screenSize (V2 600 900) (-200, -100)
  fancy (negate seconds) screenSize (V2 900 600) (200, 100)

  textured (Textures.Key "gj") $ \coord2d ->
    liftIO . GL.preservingMatrix $ do
      let
        tx = 160 :: Float
        ty = 0
        rd = seconds * 36 :: Float
        sx = 1.0 :: Float
        sy = 1.0
      GL.rotate rd $ GL.Vector3 0 0 (-1)
      GL.translate $ GL.Vector3 tx ty 0
      GL.scale @Float sx sy 1.0

      Lib.drawQuads coord2d $
        Lib.quad 320 320 0 0

  textured (Textures.Key "gj") $ \coord2d ->
    liftIO . GL.preservingMatrix $ do
      let
        tx = 160 :: Float
        ty = 0
        rd = (5 + seconds) * 36 :: Float
        sx = 1.0 :: Float
        sy = 1.0
      GL.rotate rd $ GL.Vector3 0 0 (-1)
      GL.translate $ GL.Vector3 tx ty 0
      GL.scale @Float sx sy 1.0

      Lib.drawQuads coord2d $
        Lib.quad 320 320 0 0

  -- XXX: use Cursor component
  Components.Position mousePos <- Apecs.get Apecs.global
  let mouseRel = mousePos / screenSize

  -- Spelunky tile gadget
  tilemap spelunky (V4 320 320 (-600) (-100)) (mouseRel - 0.25)
  let glow = 0.5 + sin (seconds * pi) * 0.5
  sprite
    [ Outline $ GL.Vector4 1.0 0.5 0.0 glow
    ]
    (Textures.Key "datapad")
    (V2 256 256)
    (V2 (-600) (-100))

  -- LTTP tile gadget
  let lttpQ@(V4 lttpW lttpH lttpX lttpY) = V4 654 321 400 (-321 / 2)
  fancy seconds screenSize (V2 lttpH lttpH ^* 1.5) (lttpX + lttpW / 2, lttpY)
  tilemap lttp lttpQ (mouseRel)
  sprite
    mempty
    (Textures.Key "bg")
    (V2 lttpH lttpH)
    (lttpQ ^. _zw)

data SpriteEffects
  = Outline (GL.Vector4 Float)

sprite
  :: ( Apecs.Has w IO Programs.Programs
     , Apecs.Has w IO Textures.Textures
     )
  => [SpriteEffects]
  -> Textures.Key
  -> V2 Float
  -> V2 Float
  -> Apecs.SystemT w IO ()
sprite effects key size pos = do
  Textures.Texture{textureObject} <- Textures.get key

  Programs.withCompiled program $ \setUniform withAttribute -> do
    GL.activeTexture $= GL.TextureUnit 0
    GL.textureBinding GL.Texture2D $= Just textureObject
    setUniform "mytexture" $ GL.TextureUnit 0

    withAttribute "texcoord" $ \texcoord ->
      Lib.withVertexAttribArray texcoord texVertices $
        withAttribute "coord2d" $ \coord2d ->
          Lib.drawQuads coord2d $
            -- XXX: what about ModelView matrix?
            -- pass mat4 instead of pos?
            Lib.quad width height x y

  mapM_ postprocess effects

  where
    program = Programs.Key "texture"

    texVertices = Vector.fromList @Float
      [ 0, 1
      , 0, 0
      , 1, 0
      , 1, 1
      ]

    V2 width height = size
    V2 x y = pos

    postprocess = \case
      Outline color ->
        outline key size color pos

solid
  :: Apecs.Has w IO Programs.Programs
  => GL.Vector4 Float
  -> (GL.AttribLocation -> Apecs.SystemT w IO ())
  -> Apecs.SystemT w IO ()
solid color4 action =
  Programs.withCompiled (Programs.Key "solid") $ \setUniform withAttribute -> do
    setUniform "mycolor" color4
    withAttribute "coord2d" action

textured
  :: ( Apecs.Has w IO Programs.Programs
     , Apecs.Has w IO Textures.Textures
     )
  => Textures.Key
  -> (GL.AttribLocation -> Apecs.SystemT w IO ())
  -> Apecs.SystemT w IO ()
textured key action =
  Programs.withCompiled (Programs.Key "texture") $ \setUniform withAttribute -> do
    Textures.Texture{textureObject} <- Textures.get key

    GL.activeTexture $= GL.TextureUnit 0
    GL.textureBinding GL.Texture2D $= Just textureObject
    setUniform "mytexture" $ GL.TextureUnit 0

    withAttribute "texcoord" $ \texcoord ->
      Lib.withVertexAttribArray texcoord texVertices $
        withAttribute "coord2d" action

  where
    texVertices = Vector.fromList @Float
      [ 0, 1
      , 0, 0
      , 1, 0
      , 1, 1
      ]

outline
  :: ( Apecs.Has w IO Programs.Programs
     , Apecs.Has w IO Textures.Textures
     )
  => Textures.Key
  -> V2 Float
  -> GL.Vector4 Float
  -> V2 Float
  -> Apecs.SystemT w IO ()
outline key size color pos = do
  Textures.Texture{..} <- Textures.get key

  Programs.withCompiled program $ \setUniform withAttribute -> do
    GL.activeTexture $= GL.TextureUnit 0
    GL.textureBinding GL.Texture2D $= Just textureObject
    setUniform "mytexture" $ GL.TextureUnit 0
    setUniform "color" color

    setUniform "step_size" $
      GL.Vector2 @Float
        (1 / fromIntegral textureWidth)
        (1 / fromIntegral textureHeight)

    withAttribute "texcoord" $ \texcoord ->
      Lib.withVertexAttribArray texcoord texVertices $
        withAttribute "coord2d" $ \coord2d ->
          Lib.drawQuads coord2d $
            -- XXX: what about ModelView matrix?
            -- pass mat4 instead of pos?
            Lib.quad width height x y

  where
    program = Programs.Key "outline"

    texVertices = Vector.fromList @Float
      [ 0, 1
      , 0, 0
      , 1, 0
      , 1, 1
      ]

    V2 width height = size
    V2 x y = pos

fancy
  :: Apecs.Has w IO Programs.Programs
  => Float
  -> V2 Float
  -> V2 Float
  -> (Float, Float)
  -> Apecs.SystemT w IO ()
fancy seconds screenSize quadSize offset =
  Programs.withCompiled (Programs.Key "fancy") $ \setUniform withAttribute -> do
    setUniform "u_time" seconds
    setUniform "u_resolution" $ toGL screenSize
    setUniform "u_size" $ toGL quadSize
    setUniform "u_offset" $ toGL offset

    withAttribute "texcoord" $ \texcoord ->
      Lib.withVertexAttribArray texcoord texVertices $
        withAttribute "coord2d" $ \coord2d ->
          Lib.drawQuads coord2d $
            Lib.quad width height right top
  where
    V2 width height = quadSize
    (right, top) = offset

    texVertices = Vector.fromList @Float
      [ 0, 0
      , 1, 0
      , 1, 1
      , 0, 1
      ]

data Tileset = Tileset
  { tilesetSprites  :: Textures.Key
  , tilesetTileSize :: Float
  , tilesetLayers   :: [Layer]
  } deriving (Show)

data Layer = Layer
  { layerMap         :: Textures.Key
  , layerRepeat      :: Bool
  , layerScrollScale :: V2 Float
  } deriving (Show)

spelunky :: Tileset
spelunky = Tileset
  { tilesetSprites  = Textures.Key "spelunky-tiles"
  , tilesetTileSize = 16.0
  , tilesetLayers =
      [ Layer
          { layerMap         = Textures.Key "spelunky-layer0"
          , layerRepeat      = True
          , layerScrollScale = 2.0
          }
      , Layer
          { layerMap         = Textures.Key "spelunky-layer1"
          , layerRepeat      = False
          , layerScrollScale = 1.0
          }
      ]
  }

lttp :: Tileset
lttp = Tileset
  { tilesetSprites  = Textures.Key "lttp-tiles"
  , tilesetTileSize = 16.0
  , tilesetLayers =
      [ Layer
          { layerMap         = Textures.Key "lttp-all"
          , layerRepeat      = False
          , layerScrollScale = 1.0
          }
      ]
  }

tilemap
  :: (R2 viewport, Lib.ToQuad (viewport Float))
  => Tileset
  -> viewport Float
  -> V2 Float
  -> Apecs.SystemT Components.World IO ()
tilemap Tileset{..} viewport offset =
  Programs.withCompiled (Programs.Key "tilemap") $ \setUniform withAttribute -> do
    tileset <- Textures.get tilesetSprites
    let
      tilesetSize = V2
        (fromIntegral $ Textures.textureWidth tileset)
        (fromIntegral $ Textures.textureHeight tileset)
      tilesetSizeTiles = tilesetSize ^* recip tilesetTileSize
    setUniform "tileSize" tilesetTileSize
    setUniform "inverseTileSize" (recip tilesetTileSize)

    setUniform "tilesetSize" $ toGL tilesetSize
    setUniform "tilesetSizeTiles" $ toGL tilesetSizeTiles
    setUniform "inverseSpriteTextureSize" $ toGL (fmap recip tilesetSize)

    GL.activeTexture $= GL.TextureUnit 0
    GL.textureBinding GL.Texture2D $= Just (Textures.textureObject tileset)
    setUniform "tileset" $ GL.TextureUnit 0

    withAttribute "texture" $ \texture ->
      Lib.withVertexAttribArray texture texVertices $
        withAttribute "position" $ \position ->
          for_ tilesetLayers $ \Layer{..} -> do
          let V2 layerScrollScaleX layerScrollScaleY = layerScrollScale
          setUniform "repeatTiles" $ toGL layerRepeat

          -- XXX: MUST be filtered with NEAREST or tile lookup fails
          -- TODO: repeat / clamp_to_edge selector
          mapTexture <- Textures.get layerMap
          let
            mapSizeTiles = V2 @Float
              (fromIntegral $ Textures.textureWidth mapTexture)
              (fromIntegral $ Textures.textureHeight mapTexture)

          GL.activeTexture $= GL.TextureUnit 1
          GL.textureBinding GL.Texture2D $= Just (Textures.textureObject mapTexture)
          setUniform "layermap" $ GL.TextureUnit 1
          setUniform "mapSizeTiles" $ toGL mapSizeTiles
          setUniform "inverseTileTextureSize" $ toGL (fmap recip mapSizeTiles)

          setUniform "viewportSize" $ toGL (viewport ^. _xy)

          let
            V2 x y = offset * mapSizeTiles ^* tilesetTileSize

            viewOffset = V2 @Float
              (fromIntegral @Int . floor @Float $ x * layerScrollScaleX)
              (fromIntegral @Int . floor @Float $ y * layerScrollScaleY)

          setUniform "viewOffset" $ toGL viewOffset

          Lib.drawQuads position $ Lib.toQuad viewport

  where
    texVertices = Vector.fromList @Float
      [ 0, 1
      , 0, 0
      , 1, 0
      , 1, 1
      ]

class ToGL a where
  type GLType a
  toGL :: a -> GLType a

instance ToGL (GL.Vector2 a) where
  type GLType (GL.Vector2 a) = GL.Vector2 a

  toGL = id
  {-# INLINE toGL #-}

instance ToGL (V2 a) where
  type GLType (V2 a) = GL.Vector2 a

  toGL (V2 x y) = GL.Vector2 x y
  {-# INLINE toGL #-}

instance ToGL (a, a) where
  type GLType (a, a) = GL.Vector2 a

  toGL (x, y) = GL.Vector2 x y
  {-# INLINE toGL #-}

instance ToGL Bool where
  type GLType Bool = Float
  toGL b = if b then 1.0 else 0.0
  {-# INLINE toGL #-}
