module Components.Textures
  ( Textures(..)
  , Key(..)
  , Texture(..)

    -- * Initialization
  , loadAll
  , loadTexture

    -- * Usage
  , get
  ) where

import Apecs (Component(..), SystemT, Global)
import Control.Exception (bracket, bracket_)
import Data.Foldable (for_)
import Data.Map.Strict (Map)
import Data.StateVar (($=))
import Debug.Trace (traceM)
import Foreign
import GHC.Stack (HasCallStack)
import Linear (V2(..))
import System.Directory
import System.FilePath

import qualified Apecs
import qualified Data.Map.Strict as Map
import qualified Graphics.Rendering.OpenGL as GL
import qualified SDL
import qualified SDL.Image

newtype Textures = Textures
  { unTextures :: Map Key Texture
  }
  deriving stock (Show)
  deriving newtype (Semigroup, Monoid)

instance Component Textures where
  type Storage Textures = Global Textures

-- TODO: generate Yesod-style static keys
newtype Key = Key
  { unKey :: FilePath
  } deriving (Eq, Ord, Show)

data Texture = Texture
  { textureSource :: FilePath
  , textureWidth  :: Int32
  , textureHeight :: Int32
  , textureObject :: GL.TextureObject
  } deriving (Eq, Ord, Show)

-- * Initialization

textureData :: FilePath
textureData = "data" </> "textures"

loadAll :: (Apecs.Has w IO Textures) => SystemT w IO ()
loadAll = walkFrom textureData
  where
    walkFrom current = do
      names <- Apecs.liftIO $ listDirectory current
      for_ names $ \name -> do
        let next = current </> name
        isDir <- Apecs.liftIO $ doesDirectoryExist next
        if isDir then
          walkFrom next
        else do
          let textureId = Key . dropExtension $ makeRelative textureData next
          texture <- Apecs.liftIO $ loadTexture next
          traceM $ "Loaded texture as " <> show (unKey textureId)
          traceM $ show texture
          Apecs.modify Apecs.global $
            Textures . Map.insert textureId texture . unTextures

loadTexture :: FilePath -> IO Texture
loadTexture source = do
  [tex] <- GL.genObjectNames 1

  loadImage source $ \imgWidth imgHeight imgPtr -> do
    let target = GL.Texture2D
    GL.textureBinding  target      $= Just tex
    GL.texture         target      $= GL.Enabled
    GL.textureFilter   target      $= ((GL.Nearest, Nothing), GL.Nearest)
    GL.textureWrapMode target GL.S $= (GL.Repeated, GL.Repeat)
    GL.textureWrapMode target GL.T $= (GL.Repeated, GL.Repeat)
    -- GL.generateMipmap  target      $= GL.Enabled

    GL.texImage2D
      GL.Texture2D
      GL.NoProxy
      0
      GL.RGBA8
      (GL.TextureSize2D imgWidth imgHeight)
      0
      (GL.PixelData GL.ABGR GL.UnsignedByte imgPtr)

    -- GL.textureFilter  target $= ((GL.Nearest, Just GL.Nearest), GL.Nearest)
    GL.textureBinding target $= Nothing

    pure Texture
      { textureSource = source
      , textureWidth  = imgWidth
      , textureHeight = imgHeight
      , textureObject = tex
      }

loadImage :: Integral size => FilePath -> (size -> size -> Ptr () -> IO a) -> IO a
loadImage filePath action =
  bracket (SDL.Image.load filePath) SDL.freeSurface $ \source -> do
    V2 width height <- SDL.surfaceDimensions source
    let sdlSize = V2 (fromIntegral width) (fromIntegral height)
    bracket (SDL.createRGBSurface sdlSize SDL.RGBA8888) SDL.freeSurface $ \temporary -> do
      _nothing <- SDL.surfaceBlit source Nothing temporary Nothing
      bracket_ (SDL.lockSurface temporary) (SDL.unlockSurface temporary) $ do
        pixels <- SDL.surfacePixels temporary
        action (fromIntegral width) (fromIntegral height) pixels

-- * Usage

get :: (HasCallStack, Apecs.Has w IO Textures) => Key -> Apecs.SystemT w IO Texture
get key = do
  Textures textureMap <- Apecs.get Apecs.global
  case Map.lookup key textureMap of
    Nothing ->
      error $ "Texture not found: " <> show (unKey key)
    Just texture ->
      pure texture
